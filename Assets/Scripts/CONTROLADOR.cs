﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CONTROLADOR : MonoBehaviour
{
    public MODELO modelo;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (modelo.sumar == true)
        {
            Sumas();
        }
        else { }

        if (modelo.resta == true)
        {
            Resta();
        }

        if (modelo.x >= 100)
        {
            modelo.x = 100;
        }
        else
        { }

        if (modelo.x <= 0)
        {
            modelo.x = 0;
        }
        else { }
    }

    public void Sumas()
    {
        modelo.x += 1;

        modelo.sumar = false;
    }

    public void Resta()
    {
        modelo.x -= 1;

        modelo.resta = false;
    }

    public void botonSuma()
    {
        modelo.sumar = true;
    }

    public void botonResta()
    {
        modelo.resta = true;
    }
}
